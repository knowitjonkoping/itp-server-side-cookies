# Google Analytics Cookies Server Side

## Vad behövs att göra?
1. En subdomän, till exempel aws.domän.se
2. Lägg till ett fält i GTM med __customTask__ och välj den variabel som som värde (Kolla nedan om hur man skapar JS variablen)
3. Lägg till ett fält i GTM med __cookieUpdate__ och ha __false__ som värde 
4. Vi behöver ladda upp den PHP-kod som finns i denna repository till den subdomänen.
#
### Skapa en ny Tag eller välj en nuvarande där taggen läses på alla sidor
![Tag](doc/img/Tag.png)
#

### Skapa en ny Custom JavaScript GTM variabel

![Variable](doc/img/Variable.png)
 
JavaScript kod (se till att ändra aws.domän.se mot den riktiga):
```JavaScript
function() {
    var gaProxyEndpoint = 'https://aws.domän.se/';
    return function(customTaskModel) {
        customTaskModel.set('cookieUpdate', false);
        var originalSendHitTask = customTaskModel.get('sendHitTask');
        customTaskModel.set('sendHitTask', function(model) {
            originalSendHitTask(model);
            try {
                var gaCookieName = model.get('cookieName');
                var base64encodedPayload = btoa(model.get('hitPayload')+'&ckn='+gaCookieName);             
                var xhr = new XMLHttpRequest;
                xhr.withCredentials = true;
                xhr.open("POST", gaProxyEndpoint, false);
                xhr.send(base64encodedPayload);
            } catch (err) {
                originalSendHitTask(model);
            }
        });
    }
    ;
}
```