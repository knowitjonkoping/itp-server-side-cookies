<?php
/*
* Project title: ITP Server Side Cookies
* Langauge: PHP
* Author: fadi.chamoun@knowit.se
*/

class gaProxy {
    private $anonymizeIp = false;

    // private $propertyId = 'UA-XXXXX-XX';

    // private $cookieDomain = '.domän.se';

    private $gaEndPoint = 'https://www.google-analytics.com/collect';

    // Ändra ingenting nedan
    public function setCORS() {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Max-Age: 1800");
    }

    public function getIpAddress() {
        $ipAddress = $_SERVER['REMOTE_ADDR'] ? : ($_SERVER['HTTP_X_FORWARDED_FOR'] ? : $_SERVER['HTTP_CLIENT_IP']);
        if ($this->anonymizeIp == true) return preg_replace('/\.\d{1,3}$/', '.0', $ipAddress);
        else return $ipAddress;
    }

    public function setCookie($cid = null, $cookieName = "_ga") {
        if(!$this->cookieDomain){
		$extract = new LayerShifter\TLDExtract\Extract();
		$result = $extract->parse($_SERVER['SERVER_NAME']);
		$this->cookieDomain = '.'.$result->getRegistrableDomain();
        }
        if ($cid) setcookie($cookieName, $cid, time() + (365 * 24 * 60 * 60 * 2) , "/", $this->cookieDomain, 0);
    }

    public function processPayload() {
	require 'vendor/autoload.php';
       	$rawRequestPayload = file_get_contents('php://input');
        parse_str(base64_decode($rawRequestPayload) , $parsedPayload);

        $cookieName = $parsedPayload["ckn"];
        unset($parsedPayload["ckn"]);

        $this->setCORS();
        $this->setCookie("GA1.2.".$parsedPayload["cid"], $cookieName);

	if($this->propertyId){
	        $client = new \GuzzleHttp\Client();
        	if ($this->propertyId) $parsedPayload["tid"] = $this->propertyId;
	        if ($_SERVER["HTTP_USER_AGENT"]) $parsedPayload["ua"] = $_SERVER["HTTP_USER_AGENT"];
	        $parsedPayload["uip"] = $this->getIpAddress();
	        $url = $this->gaEndPoint . '?' . http_build_query($parsedPayload, '', '&');
        	$promise = $client->requestAsync('get', $url, ['headers' => ['User-Agent' => $parsedPayload["ua"], 'Accept-language' => $parsedPayload["ul"]]]);
	        $response = $promise->wait();

	}
	header('Content-Type: image/gif');
	die(hex2bin('47494638396101000100900000ff000000000021f90405100000002c00000000010001000002020401003b'));
    }
}
$gaProxy = new gaProxy();
$gaProxy->processPayload();
